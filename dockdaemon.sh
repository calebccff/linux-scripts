#!/bin/bash

DOCKFILE=/home/caleb/tmp/docked

function dockEvent {
	! xrandr | grep "^HDMI3" | grep disconnected
}

echo 0 > $DOCKFILE

while true
do
	sleep 1
	if dockEvent && grep -q 0 "$DOCKFILE"; then
		echo 1 > $DOCKFILE
		al-polybar-session -z $HOME/.config/polybar/sessions/docked-sessionfile
		nmcli radio wifi off
		sh $HOME/.screenlayout/docked.sh
	fi
	if grep -q 1 "$DOCKFILE" && ! dockEvent; then
		echo 0 > $DOCKFILE
		al-polybar-session
		nmcli radio wifi on
	fi
done