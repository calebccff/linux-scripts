#!/usr/bin/env python3

import subprocess, argparse, os
FNULL = open(os.devnull, "w")

connected = False

proc = subprocess.Popen(['nordvpn', 'status'], stdout=subprocess.PIPE)
output = proc.stdout.read().decode("utf-8").split("\n")
#print(output)
if "Connected" in output[0]:
    connected = True

parser = argparse.ArgumentParser(description="Script to get and set nordvpnsd status for polybar")
parser.add_argument(
    "-t",
    "--toggle",
    action="store_true"
)

args = parser.parse_args()

if args.toggle:
    subprocess.Popen(["nordvpn", "disconnect" if connected else "connect"], stdout=FNULL)

if connected:
    print(output[3][output[3].index(":")+2:]+": "+output[4][output[4].index(":")+2:], end="")
else:
    print("Not Connected", end="")
