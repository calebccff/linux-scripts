vlc \
    --no-video-deco \
    --no-embedded-video \
    --screen-fps=60 \
    --screen-top=392 \
    --screen-left=1280 \
    --screen-width=1920 \
    --screen-height=1080 \
    --meta-title=vlc-scratchpad \
    screen:// &
extramaus &
sleep 1
i3-msg '[class="vlc"] move scratchpad'